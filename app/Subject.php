<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    //
    protected $fillable =[
        'title',
        'content',
        'father'];
    protected $hidden = [
        'id',
        'created_at',
        'updated_at'];
}
