<?php

function flash($message, $level = 'custom')
{
    session()->flash('flash_message', $message);
    // danger(red), info(light blue), success(green), warning(yellow), default(grey), custom(green?, blue?), primary(blue)
    session()->flash('flash_message_level', $level);
    if ($level == 'danger' || $level == 'warning') {
        session()->flash('flash_message_title', 'Atenção!');
    } elseif ($level == 'custom' || $level == 'default' || $level == 'info' || $level == 'primary') {
        session()->flash('flash_message_title', 'Notificação:');
    } elseif ($level == 'success') {
        session()->flash('flash_message_title', 'Sucesso!');
    }
}
