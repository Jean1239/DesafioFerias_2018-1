<?php

namespace App\Helpers;

class Business extends \Faker\Provider\Base
{
    public function department()
    {
        $departments = ['Qualidade', 'Presidência', 'Adm-Fin', 'Desenvolvimento Humano', 'Projetos', 'Comunicação', 'Negócios'];
        $rand = rand(0, 6);
        return $departments[$rand];
    }
    
    public function role()
    {
        $roles = ['Assessor', 'Diretor', 'Conselheiro', 'Trainee', 'Presidente'];
        $rand = rand(0, 4);
        return $roles[$rand];
    }
}