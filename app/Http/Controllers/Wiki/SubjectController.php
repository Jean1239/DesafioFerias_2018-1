<?php

namespace App\Http\Controllers\Wiki;

use App\Http\Controllers\Controller;
use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    //
    public function index(){
        $subjects= Subject::orderBy('id','desc')->get();
        return view('wiki.index')->with(['subjects'=>$subjects]);
    }
    
    public function create(){
        return view('wiki.create');
    }
    
    public function store(Request $request){
        Subject::create([
            'title'=>$request['title'],
            'content'=>$request['content'],
            'father'=>$request['father']
            ]);
        return redirect()->route('wiki');
    }
    
    public function show($id){
        $subject=Subject::find($id);
        return view('wiki.show')->with(['subject'=>$subject]);
    }
    
    public function edit($id){
        $subject=Subject::find($id);
        return view('wiki.edit')->with(['subject'=>$subject]);
    }
    
    public function update(Request $request, $id){
        $subject= Subject::find($id);
        $subject->title= $request['title'];
        $subject->father= $request['father'];
        $subject->content=$request['content'];
        $subject-> save();
        return redirect()->route('wiki');
    }
    
    public function destroy($id){
        $subject=Subject::find($id);
        $subject->delete();
        return redirect()->route('wiki');
    }
}
