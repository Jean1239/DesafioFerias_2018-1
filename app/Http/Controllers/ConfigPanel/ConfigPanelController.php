<?php

namespace App\Http\Controllers\ConfigPanel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class ConfigPanelController extends Controller
{
    public function index()
    {   

        return view('config_panel.layout');
    }
}
