<?php

namespace App\Http\Controllers\Group;

use Illuminate\Http\Request;
use App\Group;
use App\JobRole;
use App\GroupCategory;
use App\User;
use App\Http\Requests;
use App\Http\Requests\GroupRequest;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::get();
        $group_categories = GroupCategory::pluck('name', 'id');
        $users = User::pluck('name', 'id');

        return view('config_panel/group/index', compact('groups', 'group_categories', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupRequest $request)
    {
        $group = Group::create($request->all());
        if (count($request->user_list)) {
            $group->users()->sync($request->input('user_list'));
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::FindOrFail($id);
        $group_categories = GroupCategory::pluck('name', 'id');
        $users = User::pluck('name', 'id');
        $group_users = $group->users()->get();
        $jobroles = JobRole::get()->all();

        return view('config_panel/group/edit', compact('group', 'group_categories', 'users', 'group_users', 'jobroles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GroupRequest $request, $id)
    {
        $group = Group::FindOrFail($id);
        $group->update($request->all());
        if (count($request->user_list)) {
            $group->users()->sync($request->input('user_list'));
        }

        return redirect('/config/groups');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Group::destroy($id);

        return redirect('/config/groups');
    }

    public function setJobRole(Request $request)
    {
      $group_user = explode('-', $request->data);

      $group = Group::find($group_user[1]);
      $group = $group->users()->find($group_user[0]);
      $group->pivot->job_role_id = ($group_user[2] ? $group_user[2] : null);
      $group->pivot->save();
    }
}
