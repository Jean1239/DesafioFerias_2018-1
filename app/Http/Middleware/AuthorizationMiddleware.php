<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\User;

class AuthorizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if($request->route())
        {
            $route = $request->route()->getName();
            // Perform action
            if(!hasPermission($route)){
                if(!\Auth::check()){
                    \Session::put('url.intended', $request->route()->getPath());
                    return redirect()->guest('login');
                } else {
                    return abort(403, 'Unauthorized action.');
                }
            }
        }
        return $response;
    }
}
