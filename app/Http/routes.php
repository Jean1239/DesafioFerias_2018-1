<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'web'], function () {

    //Home
    Route::get('/', ['as' => 'home.index', 'uses' => 'Home\IndexController@index']);
  
    // Authentication routes...
    Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('login', ['as' => 'auth.login', 'uses' => 'Auth\LoginController@login']);
    Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\LoginController@logout']);

    //Profile routes...
    Route::get('auth/profile', ['as' => 'auth.profile.index', 'uses' => 'Auth\ProfileController@index']);
    Route::get('auth/profile/edit', ['as' => 'auth.profile.edit', 'uses' => 'Auth\ProfileController@edit']);
    Route::post('auth/profile', ['as' => 'auth.profile.update', 'uses' => 'Auth\ProfileController@update']);
	Route::post('auth/profile/password', ['as' => 'auth.profile.update', 'uses' => 'Auth\ProfileController@updatePassword']);

  	//Painel de controle
  	Route::group(['as' => 'config.', 'prefix' => 'config'],  function() {
        Route::get('', 'ConfigPanel\ConfigPanelController@index');
        Route::get('/ej/{id}/show', 'Ej\EjController@show');
        Route::get('/ej/{id}/edit', 'Ej\EjController@edit');
        
        Route::resource('users', 'User\UserController');
        Route::get('/user/{id}/edit', 'User\UserController@edit');
        
        Route::group(['as' => 'groups.', 'prefix' => 'groups'],  function() {
        	Route::resource('category', 'Group\GroupCategoryController');
            Route::put('/{id}/edit/setJobRole', 'Group\GroupController@setJobRole');
        });
        
        Route::resource('groups', 'Group\GroupController');
        Route::resource('/job_roles', 'User\JobRolesController');
    });

    //User routes...
    Route::get('user', 'User\UserController@showAll');
    Route::get('user/{id}', 'User\UserController@show');

    //Ej routes...
    Route::resource('ejs', 'Ej\EjController');
    
    Route::get('/wiki', 'Wiki\SubjectController@index')->name('wiki');
    Route::get('/wiki/new', 'Wiki\SubjectController@create')->name('subject.create');
    Route::post('/wiki/new', 'Wiki\SubjectController@store')->name('subject.store');
    Route::get('/wiki/read/{id}', 'Wiki\SubjectController@show')->name('subject.show');
    Route::get('/wiki/edit/{id}', 'Wiki\SubjectController@edit')->name('subject.edit');
    Route::post('/wiki/edit/{id}', 'Wiki\SubjectController@update')->name('subject.update');
    Route::post('/wiki/destroy/{id}', 'Wiki\SubjectController@destroy')->name('subject.destroy');
});
