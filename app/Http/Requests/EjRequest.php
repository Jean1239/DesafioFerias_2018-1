<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EjRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:64', 
            'about', 
            'area' => 'required|max:128', 
            'site' => 'max:255', 
            'email' => 'required|email|max:128|unique:ejs', 
            'state' => 'required|max:64', 
            'city' => 'required|max:64', 
            'address' => 'required|max:128', 
            'university' => 'required|max:128', 
            'majors' => 'max:255', 
            'facebook' => 'max:255', 
            'phone' => 'max:18',
        ];
    }
}
