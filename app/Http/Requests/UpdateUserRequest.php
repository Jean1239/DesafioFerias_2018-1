<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $emailId = \App\User::where('email', $_POST['email'])->first()->id;
        return [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users,email,'.$emailId,
            'username'=> 'unique:users',
        ];
    }
}
