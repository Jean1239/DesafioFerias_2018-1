<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupCategory extends Model
{
    protected $fillable = ['name'];

    /**
     * Relationships 
    */
    public function groups()
    {
        return $this->hasMany('App\Group');
    }
}
