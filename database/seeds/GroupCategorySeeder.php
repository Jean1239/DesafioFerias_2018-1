<?php

use Illuminate\Database\Seeder;

class GroupCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group_categories = factory(App\GroupCategory::class, 5)->create();
    }
}
