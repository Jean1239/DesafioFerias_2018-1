<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEjsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ejs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->text('about');
            $table->string('area', 128);
            $table->string('site', 255);
            $table->string('email', 128)->unique();
            $table->string('state', 64);
            $table->string('city', 64);
            $table->string('address', 128);
            $table->string('university', 128);
            $table->string('majors', 255);
            $table->string('facebook', 255);
            $table->string('phone', 18);
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ejs');
    }
}
