// USES JQUERY

/*global $*/

var __popover_template = "#popover-template";
var __popover_titles;
var __popover_contents;
var __popover_control;

$(document).ready(function() {
	popover_module_init();
});

function popover_module_init()
{
	var index = 0;
	__popover_contents = new Array();
	__popover_titles = new Array();
	__popover_control = new Array();

	$("[data-toggle=popover-module]").each(function(key, elem) {
		__popover_titles.push($(elem).data('title'));
		__popover_contents.push($(elem).data('content'));
		$(elem).data('content', index);
		$(elem).data('title', index);
		$(elem).attr('data-content', index);
		$(elem).attr('data-title', index++);

		switch($(elem).data('trigger'))
		{
			case 'hover':
				var trigger = 'mouseenter';
				var untrigger = 'mouseleave';

				$(elem).click(function() {
					var status = $(this).data('fixed');
					$(this).data('fixed', !status);
				});

				$(elem).on(trigger, function() {
					if($(elem).parent().find(".popover-module").size() == 0)
					{
						var popup = create_popover(this);
						
						// Shows popup
						$(this).after(popup);

						// Finishes positioning popup
						show_popover(elem, popup);
					}
				});

				$(elem).on(untrigger, function() {
					// If popup is not fixed
					if(!$(this).data('fixed'))
					{
						// Hides popup
						var popup = $(this).next();
	
						$(popup).addClass('popover-hidden');
						setTimeout(function(){clear_popover(popup)}, 500);
					}
				});
			break;
		}
	});
};

function clear_popover(popover)
{
	$(popover).remove();
}

function create_popover(target)
{
	var popover = document.createElement('div');

	var title = __popover_titles[$(target).data('title')];
	var content = __popover_contents[$(target).data('content')];
	popover.innerHTML = $(__popover_template).html();

	if(content != '')
		$(popover).find("div.popover-module-content").html(content);

	if(title != '')
		$(popover).find("div.popover-module-title").html(title);

	$(popover).addClass("popover-module popover-hidden");
	$(popover).data('fixed', false);

	return popover;
}

function show_popover(target, popup)
{
	var pos = $(target).offset();
	pos.left += $(target).outerWidth / 2;
	pos.top -= $(popup).height() + 20;

	$(popup).css("left", pos.left);
	$(popup).css("top", pos.top);
	$(popup).removeClass("popover-hidden");
}