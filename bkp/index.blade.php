@extends('layout')



@section('content')
<!-- Novo formulário -->
        
<div class="row">
<div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Configurações <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
            <ul class="dropdown-menu drop-menu-right" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
            </ul>
        </div>

		<h4 class="page-title">Membros</h4>
		<ol class="breadcrumb">
			<li>
				<a href="#">Midas</a>
			</li>
			<li class="active">
				Membros
			</li>
		</ol>
	</div>
</div>
<div class="card-box" style="padding-bottom: 350px;">
    <h4 class="m-t-0 header-title"><b>Inserir novo membro</b></h4>
    <br>
    <div class="col-md-12">
    	<form class="form-horizontal" role="form">                                    
            <div class="form-group">
                <label class="col-md-2 control-label">Nome</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="Nome">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="example-email">Email</label>
                <div class="col-md-10">
                    <input type="email" id="example-email" name="example-email" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Senha</label>
                <div class="col-md-10">
                    <input type="password" class="form-control" placeholder = "Deixe em branco para gerar uma senha automática e enviar por email">
                </div>
            </div>
                                     
            <div class="form-group">
    			<label class="control-label col-sm-4">Data em que entrou na empresa</label>
    			<div class="col-sm-8">
    				<div class="input-group">
						<input type="text" class="form-control" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
						<span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
					</div><!-- input-group -->
    			</div>
    		</div>      
    		
            <div class="form-group">
                <label class="col-md-2 control-label">Text area</label>
                <div class="col-md-10">
                    <textarea class="form-control" rows="5"></textarea>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- final do inserir membro --> 

<!-- início da exibição de membros -->
<div class="card-box" style="padding-bottom: 250px;">
    <div class="col-lg-12">
    	<h4 class="m-t-0 header-title"><b>Lista de membros</b></h4>
    	<p class="text-muted font-13">
    	</p>
    	
    	<div class="p-20">
    		<table class="table table-striped m-0">
    			<thead>
    				<tr>
    					<th>#</th>
    					<th>Nome</th>
    					<th>Email</th>
    					<th>Cargo</th>
    					<th>Horários</th>
    					<th>Data em que entrou na empresa</th>
    					<th> </th>
    					<th> </th>
    				</tr>
    			</thead>
    			<tbody>
    				<tr>
    					<th scope="row">1</th>
    					<td>Mark</td>
    					<td>mark@ecomp.co</td>
    					<td>Assessor de Qualidade</td>
    				    <td>Exemplo</td>
    				    <td>25/08/2016</td>
    				    <td>
    				        <button type="button" class="btn btn-danger waves-effect waves-light">
                               <span class="btn-label"><i class="fa fa-times"></i>
                               </span>Apagar
                            </button>
                        </td>
    				    <td>
    				        <button type="button" class="btn btn-info waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-exclamation"></i>
                                </span>Editar
                            </button>
    				    </td>
    				</tr>
    				    				<tr>
    					<th scope="row">1</th>
    					<td>Mark</td>
    					<td>mark@ecomp.co</td>
    					<td>Assessor de Qualidade</td>
    				    <td>Exemplo</td>
    				    <td>25/08/2016</td>
    				    <td>
    				        <button type="button" class="btn btn-danger waves-effect waves-light">
                               <span class="btn-label"><i class="fa fa-times"></i>
                               </span>Apagar
                            </button>
                        </td>
    				    <td>
    				        <button type="button" class="btn btn-info waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-exclamation"></i>
                                </span>Editar
                            </button>
    				    </td>
    				</tr>
    				    				<tr>
    					<th scope="row">1</th>
    					<td>Mark</td>
    					<td>mark@ecomp.co</td>
    					<td>Assessor de Qualidade</td>
    				    <td>Exemplo</td>
    				    <td>25/08/2016</td>
    				    <td>
    				        <button type="button" class="btn btn-danger waves-effect waves-light">
                               <span class="btn-label"><i class="fa fa-times"></i>
                               </span>Apagar
                            </button>
                        </td>
    				    <td>
    				        <button type="button" class="btn btn-info waves-effect waves-light">
                                <span class="btn-label"><i class="fa fa-exclamation"></i>
                                </span>Editar
                            </button>
    				    </td>
    				</tr>
    			</tbody>
    		</table>
    	</div>
    
    </div>
</div>
<!-- final da exibição dos membros !-->

<!-- Fim do novo formulário !-->

<!-- Versão antiga do formulário:-->

<!--<div class="page-header">-->
<!--    <h1>Colaboradores</h1>-->
<!--</div>-->
<!--@if (hasPermission('user.store'))-->
<!--<div class="row">-->
<!--    <div class="col-md-6">-->
<!--        <div class="page-header">-->
<!--            <h2>Adicionar um Colaborador</h2>-->
<!--        </div>-->
<!--        {!! Form::open() !!}-->
<!--            @include('user.form', ['text' => ", deixe em branco para gerar e enviar por email ao colaborador"])-->
<!--            <div class=form-group>-->
<!--                {!! Form::submit('Adicionar', ['class' => 'btn btn-success']) !!}-->
<!--            </div>-->
<!--        {!! Form::close() !!}-->
<!--    </div>-->
<!--</div>-->
<!--<hr>-->
<!--@endif-->
<!--<table class="table table-hover data-table">-->
<!--    <thead>-->
<!--        <tr>-->
<!--            <th>Nome</th>-->
<!--            <th>Email</th>-->
<!--            <th>Cargos</th>-->
<!--            <th>Horários</th>-->
<!--            <th>Data que entrou na Empresa</th>-->
<!--            <th></th>-->
<!--            <th></th>-->
<!--        </tr>-->
<!--    </thead>-->
<!--    <tbody>-->
<!--    @foreach ($users as $user)-->
<!--    <tr>-->
<!--        <td>{{ $user->name }}</td>-->
<!--        <td>{{ $user->email }}</td>-->
<!--        <td>-->
<!--            {{ $user->roles->-->
<!--                filter(function($value) {-->
<!--                    return $value->name != 'guest';-->
<!--                })->implode('name', ', ') }}-->
<!--        </td>-->
<!--        <td>-->
<!--            @foreach($user->timesheet as $timetable)-->
<!--            {{ $timetable->weekday->name }}: {{ $timetable->start }} - {{ $timetable->end }} <br>-->
<!--            @endforeach-->
<!--        </td>-->
<!--        <td>{{ $user->join_date }}</td>-->
<!--        <td>-->
<!--        @if (hasPermission('user.destroy'))-->
<!--        {!! Form::open(['route' => ['user.destroy', $user->id], 'method' => 'delete']) !!}-->
<!--            {!! Form::submit('Apagar', ['class'=>'btn btn-xs btn-danger']) !!}-->
<!--        {!! Form::close() !!}-->
<!--        @endif-->
<!--        </td>-->
<!--        <td>-->
<!--        @if (hasPermission('user.edit'))-->
<!--        {!! Form::open(['route' => ['user.edit', $user->id], 'method' => 'get']) !!}-->
<!--            {!! Form::submit('Editar', ['class'=>'btn btn-xs btn-default']) !!}-->
<!--        {!! Form::close() !!}-->
<!--        @endif-->
<!--        </td>-->
<!--    </tr>-->
<!--    @endforeach-->
<!--    </tbody>-->
<!--</table>-->

@stop

@section('plugins-scripts')
    <script>
        var resizefunc = [];
    </script>
    <script src="{{ asset('/plugins/moment/moment.js')}}"></script>
 	<script src="{{ asset('/plugins/timepicker/bootstrap-timepicker.js')}}"></script>
 	<script src="{{ asset('/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js)}}"></script>
 	<script src="{{ asset('/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js)}}"></script>
 	<script src="{{ asset('/plugins/clockpicker/js/bootstrap-clockpicker.min.js)}}"></script>
 	<script src="{{ asset('/plugins/bootstrap-daterangepicker/daterangepicker.js)}}"></script>

    <script src="{{ asset('/pages/jquery.form-pickers.init.js)}}"></script>
@stop