@extends('layout')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="inline-btn">
        	<h4 class="page-title">Perfil</h4>
        	<ol class="breadcrumb">
        		<li>
        			<a href="/">Midas</a>
        		</li>
        		<li class="active">
        			Perfil
        		</li>
        	</ol>
        </div>
        <div class="btn-group pull-right m-t-15">
            <a type="button" class="btn btn-success waves-effect waves-light" aria-expanded="false" href="/auth/profile/edit">Atualizar Informações <span class="m-l-5"><i class="md md-mode-edit"></i></span></a>
        </div>
        <div class="btn-group pull-right m-t-15 m-r-15">
            <a type="button" class="btn btn-success waves-effect waves-light" aria-expanded="false" href="/password/reset/{{ $user->id }}">Editar senha <span class="m-l-5"><i class="md md-mode-edit"></i></span></a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="profile-detail card-box">
            <div>
				@if ($user->image) <!-- change this after user avatar feature is created -->
		        	<img class="img-circle" src="/img/users/{{ $user->id}}.png" alt="profile-image">
				@else
					<img class="img-circle" src="{!!URL::to('/') . '/img/users/userstock.png'!!}" alt="profile-image">
				@endif

                <hr>

                <div>
                    <p class="text-muted font-13 m-l-15 m-r-15" align="left"><strong>Nome :</strong> <span class="m-l-15">{{ $user->name }}</span></p>

                    <p class="text-muted font-13 m-l-15 m-r-15" align="left"><strong>Email :</strong> <span class="m-l-15">{{ $user->email }}</span></p>

                    <p class="text-muted font-13 m-l-15 m-r-15" align="left"><strong>Aniversário :</strong> <span class="m-l-15">{{ $user->birthday }}</span></p>

                    <p class="text-muted font-13 m-l-15 m-r-15" align="left"><strong>Membro desde :</strong> <span class="m-l-15">{{ $user->join_date }}</span></p>
                    
                    <p class="text-muted font-13 m-l-15 m-r-15" align="left"><strong>Cargo(s) :</strong> <span class="m-l-15">
						{{ $user->roles->filter(function($value) {
					        return $value->name != 'guest';
					    })->implode('name', ', ') }}
					</span></p>
                    <p class="text-muted font-13 m-l-15 m-r-15" align="left"><strong>Grupo(s) :</strong> <span class="m-l-15">
						{{ $user->groups->implode('name', ', ') }}
					</span></p>

                </div>


                <div class="button-list m-t-20">
                    <a type="button" class="btn btn-facebook waves-effect waves-light" href="http://www.facebook.com/{{ $user->facebook }}">
                       <i class="fa fa-facebook"></i>
                    </a>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
@stop
