@extends('config_panel.layout')

@section('css')

    <link href="{{ asset('dashboard/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/clockpicker/css/bootstrap-clockpicker.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

@endsection

@section('content')

<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Atualizar Usuário</h4>
        <ol class="breadcrumb">
            <li>
                <a href="/">Midas</a>
            </li>
            <li>
                <a href="/user">Membros</a>
            </li>
            <li class="active">
                Atualizar Usuário
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="m-t-0 header-title"><b>Editar {{ $user->name }}</b></h4>
            <div class="row form-horizontal">
                {!! Form::model($user, ['action' => ['User\UserController@update', $user->id], 'method' => 'PUT','enctype' => 'multipart/form-data']) !!}
                @include('config_panel.user.form', ['text' => "Deixe em branco para manter a senha atual"])
                <div class="form-group m-b-0">
                    <div class="col-sm-offset-4 col-sm-3">
                        <button type="submit" class="btn btn-default waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-exclamation"></i>
                            </span>Editar
                        </button>
                        <a type="" class="btn btn-danger waves-effect waves-light" href="/user">
                            <span class="btn-label"><i class="fa fa-times"></i>
                            </span>Cancelar
                        </a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('plugins-scripts')

    <script src="{{ asset('dashboard/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('dashboard/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('dashboard/plugins/autocomplete/jquery.mockjax.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/plugins/autocomplete/jquery.autocomplete.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/plugins/autocomplete/countries.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/pages/autocomplete.js') }}"></script>

    <script src="{{ asset('dashboard/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <!--Filestyle-->
    <script src="{{ asset('dashboard/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
 
    <!-- Forma temporária de deixar o daterangepicker em ptbr -->
    <script src="{{ asset('js/datapickerpt-br.js')}}"></script>

    <script src="{{ asset('dashboard/pages/jquery.form-pickers.init.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/pages/jquery.form-advanced.init.js') }}"></script>
@endsection