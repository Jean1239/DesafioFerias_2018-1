
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta id="token" name="token" content="{{ csrf_token() }}">
        <meta name="description" content="">
        <meta name="author" content="Ecomp">
        <link rel="shortcut icon" href="{{ asset('/midasicon.ico')}}">

        <title>Midas</title>

        @section('plugins-top')
        @yield('plugins-top')
        <link href="{{ asset('dashboard/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/menu_2/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/menu_2/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/menu_2/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/menu_2/css/menu.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/menu_2/css/responsive.css') }}" rel="stylesheet" type="text/css" />

        <script src="{{ asset('dashboard/menu_2/js/modernizr.min.js') }}"></script>
        @yield('css')

        <link href="{{ asset('dashboard/menu_2/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />


    <!-- Custom styles for this template -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    </head>


    <body>


        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- Logo container-->
                <div class="logo">
                    <div class="topbar-left">
                        <div class="text-center">
                            <a href="/" class="logo"><span>MIDAS</span></a>
                        </div>
                    </div>
                </div>
                    <!-- End Logo container-->

                    <div class="menu-extras">

                        <ul class="nav navbar-nav navbar-right pull-right">
                           <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="{{ asset('dashboard/images/users/avatar-1.jpg') }}" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('/auth/profile') }}"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                        <li class="divider"></li>
                                        <li><a href="{{ url('/logout') }}"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                                    </ul>
                                </li>
                        </ul>

                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </div>
                    </div>

                </div>
            </div>

            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">
                            <li class="has-submenu">
                                <a href="/ejs"><i class="md-domain"></i> <span>Informaçoes da EJ</span></a>
                            </li>
                            <li class="has-submenu">
                                <a class="waves-effect" ><i class="icon-people"></i> <span>Membros</span></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="/config/users">Gerenciar Membros</a>
                                    </li>
                                    <li>
                                        <a href="/config/groups">Gerenciar Grupos</a>
                                    </li>
                                    <li>
                                        <a href="/config/job_roles">Gerenciar Cargos</a>
                                    </li>
                                    <li>
                                        <a href="/config/groups/category">Gerenciar Categorias de Grupos</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!-- End navigation menu -->
                    </div>
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->


        <div class="content-page">
            <!-- Start content -->
            <div class="wrapper">
                <div class="container">

                    <!-- Page-Title -->
                    {{-- <div class="row"> --}}
                            @yield('page-title')
                    {{-- </div> --}}

                    <!-- Page Content -->
                    @yield('content')

                </div> <!-- container -->

            </div> <!-- content -->

            <footer class="footer text-right">
              <p>&copy; Ecomp 2017</p>
            </footer>

        </div>


        <!-- jQuery  -->
        <script src="{{ asset('dashboard/menu_2/js/jquery.min.js') }}"></script>
        <script src="{{ asset('dashboard/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('dashboard/js/detect.js') }}"></script>
        <script src="{{ asset('dashboard/js/fastclick.js') }}"></script>
        <script src="{{ asset('dashboard/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('dashboard/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('dashboard/js/waves.js') }}"></script>
        <script src="{{ asset('dashboard/js/wow.min.js') }}"></script>
        <script src="{{ asset('dashboard/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('dashboard/js/jquery.scrollTo.min.js') }}"></script>

        @yield('plugins-scripts')

        <!-- App core js -->
        <script src="{{ asset('dashboard/menu_2/js/jquery.core.js') }}"></script>
        <script src="{{ asset('dashboard/menu_2/js/jquery.app.js') }}"></script>


        <script>
            @yield('js')
        </script>

    </body>
</html>
