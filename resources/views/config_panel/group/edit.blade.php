@extends('config_panel.layout')

@section('css')
    <link href="{{ asset('dashboard/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- Sweet alert -->
    <link href="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">

    <!-- Bootstrap table css -->
    <link href="{{ asset('dashboard/plugins/bootstrap-table/css/bootstrap-table.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css"  />
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Editar Grupo</h4>
        <ol class="breadcrumb">
            <li>
                <a href="/">Midas</a>
            </li>
            <li>
                <a href="/user">Membros</a>
            </li>
            <li class="active">
                Editar Grupo
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
        <h4 class="m-t-0 header-title"><b>Editar {{ $group->name }}</b></h4>
            <div class="row form-horizontal">
                {!! Form::model($group, ['action' => ['Group\GroupController@update', $group->id], 'method' => 'PUT']) !!}
                @include('config_panel.group.form', ['text' => "Deixe em branco para manter a senha atual"])
                <div class="form-group m-b-0">
                    <div class="col-sm-offset-4 col-sm-3">
                        <button type="submit" class="btn btn-default waves-effect waves-light">
                            <span class="btn-label"><i class="fa fa-exclamation"></i>
                            </span>Editar
                        </button>
                        <a type="" class="btn btn-danger waves-effect waves-light" href="/user">
                            <span class="btn-label"><i class="fa fa-times"></i>
                            </span>Cancelar
                        </a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card-box">
      <div class="panel-header">
        <h4 class="m-t-0 header-title"><b>Lista de cargos</b></h4>
      </div>
      <div class="panel-body">
        <div class="col-sm-12">
          <table id="custom-table"  data-toggle="table"
             data-toolbar="#custom-toolbar"
             data-locale="pt-BR"
             class="table-bordered ">
             <thead>
                <tr>
                  <th data-field="name" data-align="center">Nome</th>
                  <th data-field="job_role" data-align="center">Cargo</th>
                </tr>
            </thead>
            @foreach ($group_users as $user)
              <tr data-field="{{ $user->id }}">
                <td align="center" valign="middle">{{ $user->name }}</td>
                <td>
                  <select id="select-{{ $user->id }}" class="form-control input-md" onchange="setJobRole(this);">
                    <option value="{{ $user->id }}-{{ $group->id }}-0" default>Sem cargo</option>
                    @foreach ($jobroles as $jobrole)
                      <option value="{{ $user->id }}-{{ $group->id }}-{{ $jobrole->id }}"
                        {{ $group->users()->find($user->id)->pivot->job_role_id == $jobrole->id ? "selected" : "" }}>
                          {{ $jobrole->name }}
                      </option>
                    @endforeach
                  </select>
                </td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('plugins-scripts')
  <script>
    function setJobRole(selected) {
      data = selected.value;
      $.ajax({
          url: "/config/groups/{{ $group->id }}/edit/setJobRole",
          type: "PUT",
          data: {data : data},
          headers: {
              'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
          },
          cache: false,
      });
    }
  </script>

  <!-- jQuery  -->
  <script src="{{ asset('dashboard/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

  <!-- Sweet alert -->
  <script src="{{ asset('dashboard/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>

  <!-- Bootstrap plugin js -->
  <script src="{{ asset('dashboard/plugins/bootstrap-table/js/bootstrap-table.min.js') }}"></script>
  <script src="{{ asset('dashboard/pages/jquery.bs-table.js') }}"></script>
  <script src="{{ asset('dashboard/plugins/bootstrap-table/locale/bootstrap-table-pt-BR.js') }}"></script>

  <script type="text/javascript" src="{{ asset('dashboard/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
  <script src="{{ asset('dashboard/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>

  <script src="{{ asset('dashboard/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('dashboard/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
  <script src="{{ asset('dashboard/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
  <script src="{{ asset('dashboard/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('dashboard/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
  <script src="{{ asset('dashboard/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

  <script src="{{ asset('dashboard/pages/jquery.form-pickers.init.js') }}"></script>
  <script type="text/javascript" src="{{ asset('dashboard/pages/jquery.form-advanced.init.js') }}"></script>

@endsection
