@extends('config_panel.layout')
@section('plugins-top')
    <link href="{{ asset('dashboard/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/dataTables.colVis.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/css/fixedColumns.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
@stop
@section('content')

<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Categorias de grupos</h4>
        <ol class="breadcrumb">
            <li>
                <a href="/">Midas</a>
            </li>
            <li class="active">
                Categorias de grupos
            </li>
        </ol>
    </div>
</div>

@if (hasPermission('group.category.store'))
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Adicionar uma categoria</b></h4>
                <div class="row form-horizontal">
                    {!! Form::open() !!}
                    @include('config_panel.group.group_category.form')
                    <div class="form-group m-b-0">
                        <div class="col-sm-offset-4 col-sm-3">
                        {!! Form::button('Adicionar', ['type'=>'submit', 'class' => 'btn waves-effect waves-light btn-default']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 m-b-30 header-title"><b>Lista de categorias</b></h4>
            <table  id="datatable-colvid"  class="table table-striped table-bordered table-actions-bar">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Editar</th>
                        <th>Apagar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($group_categories as $group_category)
                        <tr>
                            <td>{{ $group_category->name }}</td>
                            <td align="center">
                                @if (hasPermission('config.groups.category.edit'))
                                {!! Form::open(['route' => ['config.groups.category.edit', $group_category->id], 'method' => 'get']) !!}
                                    {!! Form::button('<i class="md md-edit"></i>', array('type' => 'submit', 'class' => 'rm-button table-action-btn')) !!}
                                {!! Form::close() !!}
                                @endif
                            </td>
                            <td align="center">
                                @if (hasPermission('config.groups.category.destroy'))
                                {!! Form::open(['route' => ['config.groups.category.destroy', $group_category->id], 'method' => 'delete']) !!}
                                   {!! Form::button('<i class="md md-close"></i>', array('type' => 'submit', 'class' => 'rm-button table-action-btn')) !!}
                                {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
@section('plugins-scripts')
    <!--EX
    <script src="{{ asset('dashboard/js/jquery.min.js')}}"></script>
    LEMBRAR DE COLOCAR OS ARQUIVOS DE /datatables, /js e /pages DENTRO DE /js
    -->
    
    <script src="{{ asset('dashboard/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/jszip.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/pdfmake.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/vfs_fonts.js')}}"></script>
    <script src="{{ asset('dashboard/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/responsive.bootstrap.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.colVis.js')}}"></script>
    <script src="{{ asset('dashboard/js/dataTables.fixedColumns.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/datatables.init.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.core.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.app.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({keys: true});
            $('#datatable-responsive').DataTable();
            $('#datatable-colvid').DataTable({
                "dom": 'C<"clear">lfrtip',
                "colVis": {
                    "buttonText": "Change columns"
                }
            });
            $('#datatable-scroller').DataTable({
                ajax: "assets/plugins/datatables/json/scroller-demo.json",
                deferRender: true,
                scrollY: 380,
                scrollCollapse: true,
                scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});
            var table = $('#datatable-fixed-col').DataTable({
                scrollY: "300px",
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                fixedColumns: {
                    leftColumns: 1,
                    rightColumns: 1
                }
            });
        });
        TableManageButtons.init();
    
    </script>
@stop