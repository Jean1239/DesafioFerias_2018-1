<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta id="token" name="token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="Ecomp">
    <link rel="shortcut icon" href="{{ asset('/midasicon.ico')}}">
    
    <title>Midas</title>
    
    @section('plugins-top')
    @yield('plugins-top')
        <link href="{{ asset('dashboard/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/core.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/components.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/pages.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/responsive.css')}}" rel="stylesheet" type="text/css" />
        <script src="{{ asset('dashboard/js/modernizr.min.js')}}"></script>
    @yield('css')

    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/style.css')}}" rel="stylesheet">
  </head>
  <body role="document" class="widescreen fixed-left-void">
  <!-- Begin page -->
      <div id="wrapper" class="enlarged forced">
        <header id="top-bar">
          <!-- Top Bar Start -->
          <div class="topbar">

              <!-- LOGO -->
              <div class="topbar-left">
                  <div class="text-center">
                      <a href="/" class="logo"><span>MIDAS</span></a>
                      <!-- Image Logo here -->
                      <!--<a href="index.html" class="logo">-->
                          <!--<i class="icon-c-logo"> <img src="dashboard/images/logo_sm.png" height="42"/> </i>-->
                          <!--<span><img src="dashboard/images/logo_light.png" height="20"/></span>-->
                      <!--</a>-->
                  </div>
              </div>
              <div class="box-coment">
                  
                  
              </div>

              
              <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>
			                
			                <ul class="nav navbar-nav navbar-right pull-right">
                                @if (hasPermission('config'))
                                <li class="dropdown hidden-xs">
                                    <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
                                       role="button" ><i class="icon-settings"></i></a>
                                    <ul class="dropdown-menu">
                                    	<li><a href="/config">Painel de Controle</a></li>
                                    </ul>
                                </li>
                                @endif
                                
                                <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="{{ asset('dashboard/images/users/avatar-1.jpg') }}" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ url('/auth/profile') }}"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                        <li class="divider"></li>
                                        <li><a href="{{ url('/logout') }}"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>
        </header>
        <!-- Top Bar End -->
        <!-- ========== Left Sidebar Start ========== -->
        <header id="left-sidebar">
            <div class="left side-menu">
                <div class="sidebar-inner ">
                    <!-- Divider -->
                    <div id="sidebar-menu">
                        <ul>
                            @if (hasPermission('user.index'))
                              <li class="has_sub">
                                <a href="/user"><i class="icon-people"></i> <span>Membros</span></a>
                              </li>              
                            @endif
                            @if (hasPermission('ejs.index'))
                              <li class="has_sub">
                                <a href="/ejs"><i class="md-domain"></i> <span>Empresas Juniores</span></a>
                              </li>              
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </header>
          <!-- Left Sidebar End -->
          <!-- ============================================================== -->
          <!-- Start right Content here -->
          <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        <!-- Page-Title -->
                        {{-- <div class="row"> --}}
                                @yield('page-title')
                        {{-- </div> --}}
                        
                        <!-- Page Content -->
                        @yield('content')
                        
                    </div> <!-- container -->
                               
                </div> <!-- content -->
            
                <footer class="footer text-right">
                  <p>&copy; Ecomp 2017</p>
                </footer>

            </div>
            
            
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar nicescroll">
                <h4 class="text-center">Chat</h4>
                <div class="contact-list nicescroll">
                    <ul class="list-group contacts-list">
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{{ asset('dashboard/images/users/avatar-1.jpg')}}" alt="">
                                </div>
                                <span class="name">Chadengle</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="{{ asset('dashboard/images/users/avatar-2.jpg')}}" alt="">
                                </div>
                                <span class="name">Tomaslau</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                    </ul>  
                </div>
            </div>
            <!-- Right-bar -->  
        <!-- END wrapper -->
    <!-- Bootstrap core JavaScript
    ================================================== -->

    <script>
            var resizefunc = [];
    </script>
  
    <script>
        function insertFilterTable()
        {
            if($("table.data-table thead tr:nth-child(2)").length > 0)
            {
                $("table.data-table thead tr:nth-child(2)").remove();
                return;
            }
            var inputs = "";
            $("table.data-table th").each(function(){
                if($(this).html() != "")
                    inputs += '<th><input class="form-control" type="text" placeholder="' + $(this).html() + '"></th>';
                else
                    inputs += '<th style="min-width:68px"></th>';
            });
            $("table.data-table thead").append("<tr>" + inputs + "</tr>");
            $("table.data-table thead input").keyup(function(){
                var $rows = $('table.data-table tbody tr');
                $rows.show();
                $("table.data-table thead input").each(function(){
                    var $input = $(this),
                    $table = $('table.data-table'),
                    inputContent = $input.val().toLowerCase(),
                    column = $("table.data-table thead tr:nth-child(2) th").index($input.parents('th'));

                    var $filteredRows = $rows.filter(function(){
                    var value = $(this).find('td').eq(column).text().toLowerCase();
                        return value.indexOf(inputContent) === -1;
                    });
                    /* Clean previous no-result if exist */
                    $table.find('tbody .no-result').remove();
                    /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */

                    $filteredRows.hide();
                });
                if ($("table.data-table tbody tr:visible").length == 0) {
                      $('table.data-table').find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $('table.data-table thead tr:first-child th').length +'">Nenhum resultado encontrado</td></tr>'));
                }
            });
        }
        $(function(){
            $('<button class="btn btn-default" onclick="insertFilterTable()"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filtrar resultados</button>').insertBefore($("table.data-table"));

            $.fn.datepicker.defaults.language = 'pt-BR';
            $.fn.datepicker.defaults.format = 'dd/mm/yyyy';
            
            var highchartsOptions = Highcharts.setOptions({
              lang: {
                    loading: 'Aguarde...',
                    months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    exportButtonTitle: "Exportar",
                    printButtonTitle: "Imprimir",
                    rangeSelectorFrom: "De",
                    rangeSelectorTo: "Até",
                    rangeSelectorZoom: "Periodo",
                    downloadPNG: 'Download imagem PNG',
                    downloadJPEG: 'Download imagem JPEG',
                    downloadPDF: 'Download documento PDF',
                    downloadSVG: 'Download imagem SVG'
                    // resetZoom: "Reset",
                    // resetZoomTitle: "Reset,
                    // thousandsSep: ".",
                    // decimalPoint: ','
                    }
              }
            );

            $('[data-toggle="tooltip"]').tooltip();
            $('input[title]').tooltip();
            $('select[title]').not(".selectpicker").tooltip();
            $("input.date").mask("99/99/9999").datetimepicker({
                locale: 'pt-BR',
                format: 'DD/MM/YYYY'
            });
            $("input.datetime").mask("99/99/9999 99:99").datetimepicker({
                locale: 'pt-BR',
                format: 'DD/MM/YYYY HH:mm',
                sideBySide: true
            });
            $("input.time-h-i").mask("99:99");
        });
    </script>
    <!-- jQuery -->
    <script src="{{ asset('dashboard/js/jquery.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/detect.js')}}"></script>
    <script src="{{ asset('dashboard/js/fastclick.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.slimscroll.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.blockUI.js')}}"></script>
    <script src="{{ asset('dashboard/js/waves.js')}}"></script>
    <script src="{{ asset('dashboard/js/wow.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.nicescroll.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.scrollTo.min.js')}}"></script>
    
    @yield('plugins-scripts')

    <script src="{{ asset('dashboard/js/jquery.core.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.app.js')}}"></script>
    
    <!-- XEditable Plugin -->
    <script src="{{ asset('dashboard/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/x-editable/js/bootstrap-editable.min.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('dashboard/pages/jquery.xeditable.js') }}" type="text/javascript" ></script>

    <script>
        @yield('js')
    </script>
  </body>
</html>
