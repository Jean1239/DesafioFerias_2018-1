@extends('layout')

@section('content')

<div class="page-header">
    <h1>Empresas</h1>
</div>

@if (hasPermission('company.store'))
<div class="row">
    <div class="col-md-6">
        <div class="page-header">
            <h2>Adicionar uma Empresa</h2>
        </div>
        {!! Form::open() !!}
            @include('company.form')
            <div class=form-group>
                {!! Form::submit('Adicionar', ['class' => 'btn btn-success']) !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
<hr>
@endif
<table class="table table-hover data-table">
    <thead>
        <tr>
            <th>Nome</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach ($companies as $company)
    <tr>
        <td>{{ $company->name }}</td>
        <td>
            @if (hasPermission('company.destroy'))
            {!! Form::open(['route' => ['company.destroy', $company->id], 'method' => 'delete']) !!}
                {!! Form::submit('Apagar', ['class'=>'btn btn-xs btn-danger']) !!}
            {!! Form::close() !!}
            @endif
        </td>
        <td>
            @if (hasPermission('company.edit'))
            {!! Form::open(['route' => ['company.edit', $company->id], 'method' => 'get']) !!}
                {!! Form::submit('Editar', ['class'=>'btn btn-xs btn-default']) !!}
            {!! Form::close() !!}
            @endif
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@stop