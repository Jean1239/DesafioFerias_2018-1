@extends('layout')

@section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="inline-btn">
        	<h4 class="page-title">EJs - {{ $ej->name }}</h4>
        	<ol class="breadcrumb">
        		<li>
        			<a href="/">Midas</a>
        		</li>
        		<li>
        			<a href="/ejs">Empresas juniores</a>
        		</li>
        		<li class="active">
        			{{ $ej->name }}
        		</li>
        	</ol>
        </div>
        <div class="btn-group pull-right m-t-15">
            <a type="button" class="btn btn-success waves-effect waves-light" aria-expanded="false" href="/ejs/{{ $ej->id }}/edit">Editar <span class="m-l-5"><i class="md md-mode-edit"></i></span></a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="profile-detail card-box" >
				@if ($ej->image)
		        	<img class="img img-circle" src="{!!URL::to('/') . '/img/ejs/' . $ej->image!!}" alt="profile-image">
				@else
					<img class="img img-circle" src="{!!URL::to('/') . '/img/ejs/ejstock.png'!!}" alt="profile-image">
				@endif

                <hr>
                <div class="row">
                    <div class="col-md-3">
                        
                    </div>
                    <div class="col-md-6">
                        <h4 class="text-uppercase font-600" style="text-align: center;">Sobre a {{ $ej->name }}</h4>
            
                        <p class="text-muted font-13 m-b-30" style="text-align: center;"> {{ $ej->about }} </p>

                    </div>
                    <div class="col-md-3">
                        
                    </div>
                </div>
                
                <hr>
                <div class="row widget-inline" style="text-align: left;">
                    <div class="col-md-6 widget-inline-box">
                        <h4 class="text-uppercase font-600">Informações Gerais</h4>
                        
                        <div>
                            <p class="text-muted font-13"><strong>Nome :</strong> <span class="m-l-15">{{ $ej->name }}</span></p>
            
                            <p class="text-muted font-13"><strong>Cidade :</strong> <span class="m-l-15">{{ $ej->city }}</span></p>
            
                            <p class="text-muted font-13"><strong>Estado :</strong> <span class="m-l-15">{{ $ej->state }}</span></p>
            
                            <p class="text-muted font-13"><strong>Faculdade :</strong> <span class="m-l-15">{{ $ej->university }}</span></p>
            
                            <p class="text-muted font-13"><strong>Area de Atuacao :</strong> <span class="m-l-15">{{ $ej->area }}</span></p>
            
                            <p class="text-muted font-13"><strong>Cursos :</strong> <span class="m-l-15">{{ $ej->majors }}</span></p>
                            
                        </div>
            
            
                        
                    </div>
                    <div class="col-md-6 widget-inline-box">
                        <h4 class="text-uppercase font-600">Contato</h4>
                        <div>
                            <p class="text-muted font-13"><strong>Endereco :</strong> <span class="m-l-15">{{ $ej->address }}</span></p>
                            
                            <p class="text-muted font-13"><strong>Telefone :</strong><span class="m-l-15">{{ $ej->phone }}</span></p>
            
                            <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{ $ej->email }}</span></p>
                            
                            <p class="text-muted font-13"><strong>Site :</strong> <span class="m-l-15"><a href="{{ $ej->site }}">{{ $ej->site }}</a></span></p>
                        </div>
                        <div class="button-list m-t-20">
                            <a href="{{ $ej->facebook }}" type="button" class="btn btn-facebook waves-effect waves-light">
                               <i class="fa fa-facebook"></i>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
