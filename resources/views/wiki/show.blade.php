@extends('layout')

@section('content')
<div class="">
    <div class="" style=""></div>
    <div class="">
	    <h2>Assunto: {{ $subject->title }}</h2>
	    <h4>Pai: {{ $subject->father }}</h4>
    	<p>Texto: {{ $subject->content }}</p>
    </div>
</div>
<form method="POST" action="{{ route('subject.destroy', ['id'=>$subject->id]) }}">
    {{ csrf_field() }}
    <a href="{{ route('subject.edit', ['id'=>$subject->id]) }}">Edit</a>
    <button type="submit">Delete</button>
</form>

@endsection