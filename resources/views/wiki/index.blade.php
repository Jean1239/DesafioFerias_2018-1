@extends('layout')

@section('content')
<style>

.box-commit {
    border-left: 2px solid #0e843f;
    background-color: white;
    font-size: 14px;
    
}
.box-commit p {
    text-align: center;
}
.commit-box{
  	background-color: lightgrey;
    width: 500px;
    border: 6px solid #0e843f;
    border-radius: 5px;
    padding: 25px;
    margin: 10px;
}
.Sh-Commit a {
  color: black;
  background-position: center;
  text-align: center;
  border: 2px solid #0e843f;
  border-radius: 1px;
  display: block;
  margin-left: auto;
  margin-right: auto;
}
</style>

    <div class="fh5co-portfolio">
        @foreach($subjects as $subject)
            <div class="fh5co-portfolio-item">
              <div class="commit-box">
			           <div class="fh5co-portfolio-figure animate-box" style=""></div>
			              <div class="fh5co-portfolio-description">
            			    	<div class="box-commit">
            			    			<h2>Assunto: <p> {{ $subject->title }}</p></h2>
            				    </div>
            				    <h4>Pai: {{ $subject->father }}</h4>
            			    	<p>Texto: {{ substr($subject->content, 0, 300) . '...' }}</p>
            			    </div>
                      <div class="Sh-Commit">
            			         <a href="{{  route('subject.show', ['id'=>$subject->id])  }}">Show</a>
                      </div>
                    </div>
            	</div>
        @endforeach
        <br>
   		<a href="{{  route('subject.create')  }}">Create</a>
    </div>

@endsection
