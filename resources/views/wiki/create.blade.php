@extends('layout')

@section('content')
<div class="container">
	<form method="POST" action="{{route('subject.store')}}" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="form-group">
			<label for="title">Assunto</label>
			<input type="text" name="title" class="form-control">
		</div>

		<div class="form-group">
			<label for="father">Pai</label>
			<input type="text" name="father" class="form-control">
		</div>
			
		<div class="form-group">
			<label for="content">Texto</label>
			<textarea name="content" rows="10" class="form-control"></textarea>
		</div>

		<button type="submit" class="btn btn-primary">Publish</button>
	</form>
</div>
@endsection