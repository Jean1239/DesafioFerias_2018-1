@extends('layout')

@section('content')
<div class="container">
	<form method="POST" action="{{route('subject.update', ['id'=>$subject->id])}}" enctype="multipart/form-data">
		{{ csrf_field() }}

		<div class="form-group">
			<label for="title">Assunto</label>
			<input type="text" name="title" class="form-control" value="{{ $subject->title }}">
		</div>

		<div class="form-group">
			<label for="father">Pai</label>
			<input type="text" name="father" class="form-control" value="{{ $subject->father }}">
		</div>
			
		<div class="form-group">
			<label for="content">Texto</label>
			<textarea name="content" rows="10" class="form-control">{{ $subject->content }}</textarea>
		</div>

		<button type="submit" class="btn btn-primary">Publish</button>
	</form>
</div>
@endsection