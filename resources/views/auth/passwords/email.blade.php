@extends('auth.alayout')

@section('content')
<div class=" card-box">
	<div class="panel-heading">
		<h3 class="text-center"> Recuperar Senha </h3>
	</div>
	<div class="panel-body">
    @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
    @endif
		<form class="text-center" role="form" method="POST" action="{{ url('/password/email') }}">
	    {{ csrf_field() }}
			<div class="alert alert-info alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
					×
				</button>
				Digite seu <b>Email</b> e instruções para recuperar sua senha serão enviadas para você!
			</div>
			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} m-b-0">
				<div class="input-group">
					<input type="email" class="form-control" placeholder="Digite seu Email" required="" name="email" value="{{ old('email') }}">
					@if ($errors->has('email'))
	          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
					<span class="input-group-btn">
						<button type="submit" class="btn btn-pink w-sm waves-effect waves-light">
							Enviar
						</button>
					</span>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
