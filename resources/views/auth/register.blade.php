@extends('auth.alayout')

@section('content')
<div class=" card-box">
	<div class="panel-heading">
		<h3 class="text-center"> Registrar-se no <strong class="text-custom">Midas</strong> </h3>
	</div>

	<div class="panel-body">
		<form class="form-horizontal m-t-20" role="form" method="POST" action="{{ url('/register') }}">
            {!! csrf_field() !!}

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="text" required="" placeholder="Nome" name="name" value="{{ old('name') }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
				</div>
			</div>

			<div class="form-group ">
				<div class="col-xs-12">
					<input class="form-control" type="email" required="" placeholder="Email" name="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
				</div>
			</div>

			<div class="form-group">
				<div class="col-xs-12">
					<input class="form-control" type="password" required="" placeholder="Senha" name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-xs-12">
					<input class="form-control" type="password" required="" placeholder="Confirme sua Senha" name="password_confirmation">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
				</div>
			</div>

			<!--<div class="form-group">
				<div class="col-xs-12">
					<div class="checkbox checkbox-primary">
						<input id="checkbox-signup" type="checkbox" checked="checked">
						<label for="checkbox-signup">I accept <a href="#">Terms and Conditions</a></label>
					</div>
				</div>
			</div>-->

			<div class="form-group text-center m-t-40">
				<div class="col-xs-12">
					<button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">
						Registrar
					</button>
				</div>
			</div>

		</form>

	</div>
</div>

<div class="row">
	<div class="col-sm-12 text-center">
		<p>
			Já uma tem conta?<a href="{{ url('/login') }}" class="text-primary m-l-5"><b>Entrar</b></a>
		</p>
	</div>
</div>
@endsection
