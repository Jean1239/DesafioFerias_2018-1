<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="Ecomp">
    <link rel="shortcut icon" href="{{ asset('/midasicon.ico')}}">
    
    <title>Midas</title>

    <!-- Bootstrap core CSS -->
    <!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">-->

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">-->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.4/css/bootstrap-select.min.css">-->
    @section('plugins-top')
    @yield('plugins-top')
        <link href="{{ asset('dashboard/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/core.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/components.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/acore.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/pages.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dashboard/css/responsive.css')}}" rel="stylesheet" type="text/css" />
        <script src="{{ asset('dashboard/js/modernizr.min.js')}}"></script>
    @yield('css')

    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/style.css')}}" rel="stylesheet">
  </head>
  <body role="document" class="widescreen fixed-left-void">
    <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">
        @yield('content')
    </div>

    <!-- jQuery -->
    <script src="{{ asset('dashboard/js/jquery.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/detect.js')}}"></script>
    <script src="{{ asset('dashboard/js/fastclick.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.slimscroll.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.blockUI.js')}}"></script>
    <script src="{{ asset('dashboard/js/waves.js')}}"></script>
    <script src="{{ asset('dashboard/js/wow.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.nicescroll.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.scrollTo.min.js')}}"></script>
    
    @yield('plugins-scripts')
    <script src="{{ asset('dashboard/js/jquery.core.js')}}"></script>
    <script src="{{ asset('dashboard/js/jquery.app.js')}}"></script>
    <script>
        @yield('js')
    </script>
  </body>
</html>
